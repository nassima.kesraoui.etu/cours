# Semaine 1 - Jakarta EE et Spring

Cours

1. [Introduction Jakarta EE](#introduction-jakarta-ee)
1. [Introduction Spring](#introduction-spring)
1. [Le protocole HTTP](#le-protocole-http)
1. [La programmation de servlet](#la-programmation-de-servlet)
1. [La programmation de contrôleurs avec Spring Web MVC](#la-programmation-de-contrôleurs-avec-spring-web-mvc)
1. [Un premier programme Spring avec Spring Boot](#un-premier-programme-spring-avec-spring-boot)

Exercices et QCM

1. Exercice [Contextes Spring et injection de dépendances](exercices/spring-boot-ioc-introduction.md)
1. Exercice [Contrôleur 101](exercices/controleur-101.md)
1. [QCM Servlets et JSP](https://gitlab-classrooms.cleverapps.io/assignments/33630d61-e082-4638-8a90-a7227d0e12c0/accept)


## Introduction Jakarta EE

Jakarta EE, précédemment connu sous le nom de Java Enterprise Edition (Java EE), et encore avant connu sous le nom de J2EE, est un ensemble de spécifications et d'API pour la programmation d'applications web avec le langage Java. Jakarta EE existe depuis 1999 et continue d'évoluer en permanence.

Jakarta EE propose un ensemble d'éléments qui permettent de développer des applications web selon un modèle d'architecture logicielle dit 3-tiers. Le terme 3-tiers fait référence aux couches logicielles dites présentation, traitement, et données. Chacune de ces couches se focalise sur un aspect particulier d'une application web. Ainsi la couche de présentation concerne tous les aspects qui permettent d'interagir avec l'utilisateur, par exemple via un navigateur Internet. La couche de traitement contient l'ensemble des services et des procédures qui sont au coeur du fonctionnement métier de l'application. Enfin la couche données s'intéresse, comme son nom l'indique, aux données manipulées par l'application et qui sont souvent stockées dans une base de données (SGBD).

Regarder la vidéo suivante pour une introduction à Jakarta EE.

[![Introduction Jakarta EE](http://img.youtube.com/vi/PrdYFXEeHKE/0.jpg)](https://www.youtube.com/watch?v=PrdYFXEeHKE "Introduction Jakarta EE")

[Les transparents de la vidéo](slides/introduction-java-ee.pdf)


## Introduction Spring

[Spring](https://spring.io) est un framework pour le développement d'applications Java. Il fournit un ensemble de concepts comme l'inversion du contrôle, l'injection de dépendances et la notion d'aspects pour faciliter le développement d'applications Java. Il est essentiellement utilisé pour le développement d'applications Web mais sa généralité fait qu'il peut être utilisé pour le développement de tout type d'applications. Spring existe depuis 2003 et continue d'évoluer.

Regarder la vidéo suivante pour une introduction à Spring.

[![Introduction Spring](http://img.youtube.com/vi/dFaw8NGx6kk/0.jpg)](https://www.youtube.com/watch?v=dFaw8NGx6kk "Introduction Spring")

[Les transparents de la vidéo](slides/introduction-spring.pdf)


## Le protocole HTTP

HTTP est le protocole client-serveur qui permet de faire fonctionner le web. Il fournit, en mode requête-réponse, une façon simple d'accéder à du contenu stocké à distance sur des serveurs. Il permet également d'invoquer des services ou d'accéder à des ressources avec le style architectural REST. HTTP existe depuis 1989 et continue d'évoluer.

Le protocole est détaillé dans le document [http.md](http.md).


## La programmation de servlet

Une servlet est un programme Java qui reçoit des requêtes HTTP, les traitent et retourne des réponses. Il s'agit donc d'un moyen d'exécuter du code dans un serveur web. L'exécution des servlets est prise en charge par un conteneur de servlets. Le plus connu est [Apache Tomcat](https://tomcat.apache.org). Il en existe d'autres comme par exemple [Jetty](https://www.eclipse.org/jetty).

Regarder la vidéo suivante pour une introduction aux servlets.

[![Servlet](http://img.youtube.com/vi/F7TYXTyic60/0.jpg)](https://www.youtube.com/watch?v=F7TYXTyic60 "Servlet")

[Les transparents de la vidéo](slides/servlet.pdf)

Regarder le screencast suivant pour suivre une démonstration de la programmation de servlets.

[![Screencast servlet](http://img.youtube.com/vi/55vkYR1JnZ4/0.jpg)](https://www.youtube.com/watch?v=55vkYR1JnZ4 "Screencast servlet")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week1-sc-servlets)


## La programmation de contrôleurs avec Spring Web MVC

[Spring Web MVC](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#spring-web) est un sous-projet du framework Spring qui permet de développer des applications web. Il propose un modèle de programmation qui s'appuie sur le patron modèle-vue-contrôleur. Il met en oeuvre une servlet appelée DispatcherServlet qui aiguille les requêtes HTTP vers les contrôleurs et récupère les vues à retourner aux clients en fonction des directives des contrôleurs. Les contrôleurs utilisent un modèle pour transmettre aux vues les données qui leur sont nécessaires.

Regarder la vidéo suivante pour une introduction à Spring Web MVC.

[![Spring Web MVC](http://img.youtube.com/vi/BYFFkvgy10Q/0.jpg)](https://www.youtube.com/watch?v=BYFFkvgy10Q "Spring Web MVC")

[Les transparents de la vidéo](slides/spring-web-mvc.pdf)

Regarder le screencast suivant pour suivre une démonstration de la programmation d'une application avec Spring Web MVC.

[![Screencast Spring Web MVC](http://img.youtube.com/vi/1xAKw6tCYBc/0.jpg)](https://www.youtube.com/watch?v=1xAKw6tCYBc "Screencast Spring Web MVC")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week1-sc-spring-web-mvc)


## Un premier programme Spring avec Spring Boot

Spring Boot est un projet du framework Spring pour faciliter la configuration d'applications Spring. En lien avec le sous-projet [Spring Initializr](https://start.spring.io), il permet d'accélérer un grand nombre d'étapes du développement d'une application Spring.

Regarder la vidéo suivante pour une introduction à Spring Boot.

[![Spring Boot](http://img.youtube.com/vi/08YGlu4mq6A/0.jpg)](https://www.youtube.com/watch?v=08YGlu4mq6A "Spring Boot")

Regarder la démonstration suivante pour créer un projet Spring Boot et définir un contrôleur.

[![Spring Boot démo 1](http://img.youtube.com/vi/cW3aVy8ABGc/0.jpg)](https://www.youtube.com/watch?v=cW3aVy8ABGc "Spring Boot démo 1")

Dans la deuxième partie de la démonstration suivante, vous verrez :

- quelques élements de configuration,
- comment définir une dépendance vers le starter JPA,
- comment packager une application Spring Boot.

[![Spring Boot démo 2](http://img.youtube.com/vi/UFHXhcujb0s/0.jpg)](https://www.youtube.com/watch?v=UFHXhcujb0s "Spring Boot démo 2")
