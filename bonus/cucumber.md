# BDD / Cucumber

## Introduction

BDD signifie Behavior Driven Development.
C'est une méthode de développement qui consiste à décrire le comportement d'une application dans un langage naturel avant d'écrire le code.
Cela permet de se concentrer sur le comportement de l'application et non sur l'implémentation.
Elle est utilisé pour faciliter les échanges entre les équipes de développement et les équipes de test, et parfois même avec les clients.

Vous entendrez également parler de TDD (Test Driven Development) qui consiste à écrire les tests avant d'écrire le code.

Cucumber, quant à lui, est un framework de test BDD.
Il permet d'écrire des tests en langage naturel et de les exécuter.

## Exercice

- Créer un projet Maven

```
mvn archetype:generate                        \
   "-DarchetypeGroupId=io.cucumber"           \
   "-DarchetypeArtifactId=cucumber-archetype" \
   "-DarchetypeVersion=7.14.0"                \
   "-DgroupId=hellocucumber"                  \
   "-DartifactId=hellocucumber"               \
   "-Dpackage=hellocucumber"                  \
   "-Dversion=1.0.0-SNAPSHOT"                 \
   "-DinteractiveMode=false"
```

Le projet généré sera de la forme suivante :

```
hellocucumber
├── pom.xml
└── src
    └── test
        ├── java
        │   └── hellocucumber
        │       └── RunCucumberTest.java
        │       └── StepDefinitions.java
        └── resources
            └── hellocucumber
                └── example.feature
```

- Mette à jour le fichier `example.feature` avec le code suivant :

```gherkin
Feature: Addition
  Scenario: Ajouter deux nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 70 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être 120 sur l'écran
```

Il s'agit d'un fichier au format Gherkin, qui est un langage de spécification BDD.

- Créer un fichier `Calculator.java` dans le dossier `src/main/java/hellocucumber/Calculator.java` avec le code suivant :

- Si vous exécutez le test `RunCucumberTest`, il va échouer, mais indiquera dans la console le code Java à écrire pour faire passer le test :

```shell
Scenario: Ajouter deux nombres                  # hellocucumber/example.feature:2
  Given J'ai saisi 50 dans la calculatrice
  And J'appuie sur le bouton d'addition
  And J'ai saisi 70 dans la calculatrice
  When J'appuie sur le bouton résultat
  Then Le résultat devrait être 120 sur l'écran

io.cucumber.junit.platform.engine.UndefinedStepException: The step 'J'ai saisi 50 dans la calculatrice' and 4 other step(s) are undefined.
You can implement these steps using the snippet(s) below:

@Given("J'ai saisi {int} dans la calculatrice")
public void j_ai_saisi_dans_la_calculatrice(Integer int1) {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}
@Given("J'appuie sur le bouton d'addition")
public void j_appuie_sur_le_bouton_d_addition() {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}
@When("J'appuie sur le bouton résultat")
public void j_appuie_sur_le_bouton_résultat() {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}
@Then("Le résultat devrait être {int} sur l'écran")
public void le_résultat_devrait_être_sur_l_écran(Integer int1) {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}

[...]
```  

- Copier le code indiqué dans la console dans la classe `StepDefinitions.java`, vous avez le choix d'implémenter le test de la manière dont vous le souhaitez, mais voici un exemple :

```java
import static org.junit.jupiter.api.Assertions.assertEquals;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

    private Calculator calculator = new Calculator();
    private int result;

    @Given("J'ai saisi {int} dans la calculatrice")
    public void j_ai_saisi_dans_la_calculatrice(Integer int1) {
        calculator.enter(int1);
    }
    @Given("J'appuie sur le bouton d'addition")
    public void j_appuie_sur_le_bouton_d_addition() {
        calculator.plus();
    }
    @When("J'appuie sur le bouton résultat")
    public void j_appuie_sur_le_bouton_résultat() {
        result = calculator.result();
    }
    @Then("Le résultat devrait être {int} sur l'écran")
    public void le_résultat_devrait_être_sur_l_écran(Integer int1) {
        assertEquals(result, int1);
    }
}
```

- Le code ne compile pas, car la classe `Calculator` ne contient pas les méthodes `enter`, `plus` et `result`. Implémenter ces méthodes pour faire passer le test.
- Exécuter le test `RunCucumberTest`, il devrait passer.
- Pour aller plus loin, ajouter un nouveau scénario dans le fichier `example.feature` :

```gherkin
Feature: Addition
  Scenario: Ajouter deux nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 70 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être 120 sur l'écran

  Scenario: Ajouter trois nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 70 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 100 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être 220 sur l'écran
```

- On peut imaginer que le métier souhaite maintenant pouvoir soustraire des nombres. Ajouter un nouveau scénario dans le fichier `example.feature` :

```gherkin
Feature: Addition
  Scenario: Ajouter deux nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 70 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être 120 sur l'écran

  Scenario: Ajouter trois nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 70 dans la calculatrice
    And J'appuie sur le bouton d'addition
    And J'ai saisi 100 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être 220 sur l'écran

  Scenario: Soustraire deux nombres
    Given J'ai saisi 50 dans la calculatrice
    And J'appuie sur le bouton de soustraction
    And J'ai saisi 70 dans la calculatrice
    When J'appuie sur le bouton résultat
    Then Le résultat devrait être -20 sur l'écran
```

### Conclusion

Bravo, vous avez implémenté votre premier test BDD avec Cucumber ! Vous avez vu que la première étape consiste à écrire le test en langage naturel, puis écrire le test en Java, et enfin implémenter le code pour faire passer le test.
Cela peut paraître déroutant au début et plus long que d'écrire le code directement, mais cela permet de se concentrer sur le comportement de l'application et non sur l'implémentation.
Avec l'habitude cela devient plus rapide et plus naturel et c'est une pratique assez répendue dans les entreprises se focalisant sur le software craftmanship.

Cucumber propose les mots clés dans différentes langues, ce qui peut permettre de faire participer les clients dans l'écriture des tests.

Nous ne vous avons montré que les bases de Cucumber, mais il est possible d'aller beaucoup plus loin, notamment en utilisant des expressions régulières pour les paramètres, des tables de données, des hooks, des tags, des exemples, etc.
N'hésitez pas à consulter la documentation pour en savoir plus.

## Documentation

- [Cucumber](https://cucumber.io/)
- [Cucumber - Getting started](https://cucumber.io/docs/guides/10-minute-tutorial/?lang=java)
