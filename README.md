# Systèmes répartis avancés - 1 (SRA1)

Site de l'UE SRA1 du [Master Informatique](https://sciences-technologies.univ-lille.fr/informatique/formation/master-informatique) de l'Université de Lille.

1. Semaine 1 : [Les applications client-serveur 3-tier avec Jakarta EE et Spring](week1)
1. Semaine 2 : [La couche d'accès aux données avec JPA](week2)
1. Semaine 3 : [La couche de présentation](week3)
1. Semaines 4, 5 et 6 : Projet

# Pour aller plus loin

- [Cucumber](bonus%2Fcucumber.md)

# Nous contacter

Sur discord : https://discord.gg/R4jQACyD

Par email : 

* maximilien.chevalier@univ-lille.fr
* julien.wittouck@univ-lille.fr
* guillaume.dufrene@univ-lille.fr
* lionel.seinturier@univ-lille.fr
