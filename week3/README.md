# Semaine 3 - La couche de présentation

Il existe de nombreux frameworks pour gérer la couche de présentation d'une application web. Sans être exhaustif, on peut citer les frameworks côté client comme Agular, React, Vue.js et les frameworks côté serveur comme Thymeleaf. La présentation de ces différents frameworks ne fait pas partie des objectifs de ce cours. Nous nous limitons volontairement à présenter quelques éléments de base pour la construction de vues simples.

Cours

1. [JSP](#jsp)
1. [JSTL](#jstl)

Exercices & QCM

1. Exercice [Contrôleur Sign up](exercices/controleur-sign-up.md)
1. Exercice [Contrôleur Sign in](exercices/controleur-sign-in.md)
1. [QCM JSP/JSTL](https://gitlab-classrooms.cleverapps.io/assignments/3435ecb8-4045-41f5-aa85-e31cec747c1b/accept)


## JSP

Les JSP sont des pages web qui contiennent du code HTML et du code Java. Le code Java s'exécute côté serveur dans un conteneur comme Tomcat. Elles permettent de produire du contenu dynamique qui change selon le résultat de l'exécution du code Java.

Regarder la vidéo suivante pour une introduction aux JSP.

[![JSP](http://img.youtube.com/vi/Y4brBXyryhc/0.jpg)](https://www.youtube.com/watch?v=Y4brBXyryhc "JSP")

[Les transparents de la vidéo](slides/jsp.pdf)

Regarder le screencast suivant pour suivre une démonstration de la programmation d'une JSP.

[![Screencast JSP](http://img.youtube.com/vi/STspSQoeNbc/0.jpg)](https://www.youtube.com/watch?v=STspSQoeNbc "Screencast JSP")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week3-jsp)


## JSTL

JSTL est une librairie qui étend les fonctionnalités de JSP avec de nouvelles balises à utiliser en HTML. L'objectif est de permettre d'écrire du code plus facilement et de manière mieux intégrée avec le reste du HTML.

Regarder la vidéo suivante pour une introduction à JSTL.

[![JSTL](http://img.youtube.com/vi/23zoj1qBmFE/0.jpg)](https://www.youtube.com/watch?v=23zoj1qBmFE "JSTL")

[Les transparents de la vidéo](slides/jstl.pdf)

Regarder le screencast suivant pour suivre une démonstration de la programmation d'une JSP.

[![Screencast JSTL](http://img.youtube.com/vi/KK9pT2okFN8/0.jpg)](https://www.youtube.com/watch?v=KK9pT2okFN8 "Screencast JSTL")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week3-jstl)
