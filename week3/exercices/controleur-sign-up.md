# Contrôleur Sign up

Le but de cet exercice est de développer un contrôleur et une entité qui permettent de créer des comptes utilisateurs pour une application web.

Pour cela vous pouvez procéder en deux temps.

1. Développer une entité Account pour stocker les comptes utilisateurs avec un email, un mot de passe et un nom. Les mots de passe peuvent être chiffrés, par exemple avec MD5 ou HmacSha1.

2. Développer un contrôleur SignUpController qui, à partir d'un formulaire HTML, permet d'enregistrer un compte utilisateur. Le contrôleur retourne une vue informant l'utilisateur du succès ou de l'échec de l'enregistrement (si le compte existe déjà par exemple).
