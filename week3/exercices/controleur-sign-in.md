# Contrôleur Sign in

Le but de cet exercice est de développer un contrôleur qui permet de se connecter à partir d'un email et d'un mot de passe saisis dans un formulaire HTML. Le contrôleur retourne une vue informant l'utilisateur du succès ou de l'échec de la connexion (par exemple si le compte n'existe pas ou si le mot de passe n'est pas correct).
