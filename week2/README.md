# Semaine 2 - Accès aux données avec JPA

Cours

1. [JPA](#jpa)
1. [Spring Data](#spring-data)
1. [Notions additionnelles](#notions-additionnelles) : transaction, JQPL, API Criteria, héritage et persistence.

Exercice et QCM

1. Exercice [Modèle de données JPA pour un site de commerce en ligne](https://gitlab-classrooms.cleverapps.io/assignments/49262876-8e2f-4cd2-8c32-39b965ee8a79/accept)
1. [QCM JPA](https://gitlab-classrooms.cleverapps.io/assignments/34f91825-44c5-4ee3-8221-ea230aafb56c/accept)
1. [QCM Spring Data](https://gitlab-classrooms.cleverapps.io/assignments/977d0bb3-bbaa-4beb-8384-d1c3386b5c41/accept)


## JPA

Jakarta Persistence API (JPA), précédemment connue sous le nom Java Persistence API, est une spécification qui permet de gérer en Java des données stockées dans une base de données relationnelles. JPA est la solution de prédilection pour la mise en oeuvre de la couche d'accès aux données d'une application Spring.

Regarder la vidéo suivante pour une présentation de JPA.

[![JPA](http://img.youtube.com/vi/q5UtDiYQBgA/0.jpg)](https://www.youtube.com/watch?v=q5UtDiYQBgA "JPA")

[Les transparents de la vidéo](slides/jpa.pdf)

Regarder le screencast suivant pour une démonstration de l'utilisation de JPA.

[![Screencast JPA](http://img.youtube.com/vi/0QILkWt2Hzw/0.jpg)](https://www.youtube.com/watch?v=0QILkWt2Hzw "Screencast JPA")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week2-sc-jpa)


## Spring Data

Spring Data est un projet du framework Spring qui offre un ensemble de notions pour faciliter la gestion des sources de données JPA.

Regarder la vidéo suivante pour une présentation de Spring Data.

[![Spring Data](http://img.youtube.com/vi/bfZeyZ5BKOA/0.jpg)](https://www.youtube.com/watch?v=bfZeyZ5BKOA "Spring Data")

[Les transparents de la vidéo](slides/spring-data.pdf)

[![Screencast Spring Data](http://img.youtube.com/vi/9cyJWGng-iA/0.jpg)](https://www.youtube.com/watch?v=9cyJWGng-iA "Screencast Spring Data")

[Le code présenté dans le screencast](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week2-sc-spring-data)


## Notions additionnelles

En lien avec JPA et Spring Data, les ressources suivantes présentent :

* La notion de transaction pour exécuter de manière atomique un ensemble de mises à jour
  * une [vidéo](https://youtu.be/jP7c4yJKGAA) et ses [transparents](slides/transaction.pdf)
* JPQL pour exécuter des requêtes SQL avec JPA
  * un [screencast](https://youtu.be/AgH0nprln4c) et son [code](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week2-sc-jpql)
* L'API Criteria pour construire des requêtes de manière programmatique
  * une [vidéo](https://youtu.be/7Y931lSvj8g) et ses [transparents](slides/api-criteria.pdf)
  * un [screencast](https://youtu.be/X-e1nJKbSHY) et son [code](https://gitlab.univ-lille.fr/SRA1-2023/screencasts/week2-sc-jpql) s'appuyant sur le screencast précédent
* Le lien entre la notion d'héritage de classes et la persistence de données avec JPA
  * une [vidéo](https://youtu.be/L5Cp_DQDI-0) et ses [transparents](slides/heritage-persistence.pdf)
